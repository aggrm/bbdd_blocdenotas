/*
 * Esta clase hace la ventana del bloc de notas
 * @author Alberto Goujon Gutiérrez
 */
package bloc_de_notas;

import java.awt.FileDialog;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ventana extends javax.swing.JFrame {
    //fichero lo utilizare para obtener su ruta que tiene en el sistema operativo
    //La declaro de instancia para poder reutilizarla tanto al abrir como guardar
    String fichero;
    
    public ventana() {
        initComponents();
    }

    /**
     * Todo el codigo de acontinuacion son de los elementos creados en la ventan.
     * De la línea 27 hasta la línea 116
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        nuevoDoc = new javax.swing.JMenuItem();
        abrirDoc = new javax.swing.JMenuItem();
        guardarDoc = new javax.swing.JMenuItem();
        guardarComoDoc = new javax.swing.JMenuItem();
        anadirAlDocAnterior = new javax.swing.JMenuItem();
        salir = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE)
        );

        jMenu1.setText("Archivo");

        nuevoDoc.setText("Nuevo");
        nuevoDoc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                nuevoDocMousePressed(evt);
            }
        });
        jMenu1.add(nuevoDoc);

        abrirDoc.setText("Abrir");
        abrirDoc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                abrirDocMousePressed(evt);
            }
        });
        jMenu1.add(abrirDoc);

        guardarDoc.setText("Guardar");
        guardarDoc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                guardarDocMousePressed(evt);
            }
        });
        jMenu1.add(guardarDoc);

        guardarComoDoc.setText("Guardar Como");
        guardarComoDoc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                guardarComoDocMousePressed(evt);
            }
        });
        jMenu1.add(guardarComoDoc);

        anadirAlDocAnterior.setText("Añadir al archivo anterior");
        anadirAlDocAnterior.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                anadirAlDocAnteriorMousePressed(evt);
            }
        });
        jMenu1.add(anadirAlDocAnterior);

        salir.setText("Salir");
        salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                salirMousePressed(evt);
            }
        });
        jMenu1.add(salir);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    //------------------Fin de los elementos en pantalla------------------------
    
    //-----Todos los privates void son para el ejercicio de la Tarea1-----------
    private void nuevoDocMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nuevoDocMousePressed
        jTextArea1.setText("");                                                 //Vaciamos el texto escrito hasta ahora
    }//GEN-LAST:event_nuevoDocMousePressed

    private void abrirDocMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_abrirDocMousePressed
        FileDialog fl = new FileDialog(ventana.this, "Abrir fichero", 
                                        FileDialog.LOAD);                       //El file dialog es para que abra la ventana de windows. Asi sabre que fichero voy a abrir.
        fl.setVisible(true);
        
        if(fl.getFile() != null)
        {
            //Para obtener la ruta del fichero
            fichero = fl.getDirectory() + fl.getFile();
        }
        
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(fichero));
            StringBuilder sb = new StringBuilder();                             //StringBuilder me hace solo un string en vez de concatenaciones que me ocuparioan demasiada memoria           
            String linea = null;                                                //Guardo la info de esa linea aqui y lugo en la siguiente vuelta aado la demas info de las líneas
            
            while((linea = br.readLine()) != null)
            {               
                sb.append(linea + "\n");                                        //El sb es la concatenacion de todo el fichero en un string.
                jTextArea1.setText(sb.toString());
            }
            br.close();                                 
        }
        catch(Exception e)
        {
            System.out.println("Error al no encontrar al fichero");  
        }
    }//GEN-LAST:event_abrirDocMousePressed

    private void guardarComoDocMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_guardarComoDocMousePressed
        FileDialog fl = new FileDialog(ventana.this, "Guardar fichero como",
                                        FileDialog.SAVE);                       //El file dialog es para que abra la ventana de windows,asi sabiendo en que ruta voy a guardar el fichero
                                
        fl.setVisible(true);
        
        
        if(fl.getFile() != null)
        {
            fichero = fl.getDirectory() + fl.getFile();                         //Para obtener la ruta del fichero
        }
        try
        {
            
            BufferedWriter bw = new BufferedWriter(new FileWriter(fichero));    
            bw.write(jTextArea1.getText());
            bw.close();
        }
        catch(Exception e)
        {
            System.out.println("Error al guardar");  
        }
        
    }//GEN-LAST:event_guardarComoDocMousePressed

    private void salirMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salirMousePressed
        System.exit(0);                                                         //Esto cierra el programa
    }//GEN-LAST:event_salirMousePressed

    private void guardarDocMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_guardarDocMousePressed
        File fviejo = new File(fichero);                                        //Para poder modificar un fichero obtengo su rutta
        fviejo.delete();                                                        //Elimino ese fichero para que no se me duplique el contenido
        File fnuevo = new File(fichero);                                        //Vuelvo a crear su ruta pero el contenido esta otra vez desde 0
        String linea = jTextArea1.getText();                                    //Obtengo toda la información del texto que esta en mi text area
        
        try 
        {
            FileWriter fw = new FileWriter(fnuevo, true);
            fw.write(linea);
            fw.close();
        } 
        catch (IOException e) 
        {
            System.out.println("Error al sobre escribir un archivo");
        }
    }//GEN-LAST:event_guardarDocMousePressed

    private void anadirAlDocAnteriorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_anadirAlDocAnteriorMousePressed
        File f = new File(fichero);                                             //Para poder modificar el fichero debo obtener su ruta
        String linea = jTextArea1.getText();                                    //Obtengo toda la información del texto que esta en mi text area
         
        try 
        {
            FileWriter fw = new FileWriter(f, true);
            fw.write("\n" + linea);
            fw.close();
        } 
        catch (IOException e) 
        {
            System.out.println("Error al escribir en el fichero anterior");
        }
        
         
    }//GEN-LAST:event_anadirAlDocAnteriorMousePressed
    //----------------Fin de los privates void de la Tarea1---------------------
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem abrirDoc;
    private javax.swing.JMenuItem anadirAlDocAnterior;
    private javax.swing.JMenuItem guardarComoDoc;
    private javax.swing.JMenuItem guardarDoc;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JMenuItem nuevoDoc;
    private javax.swing.JMenuItem salir;
    // End of variables declaration//GEN-END:variables
}
